A Basic Bootstrap 4 NavWalker Menu for Sage 8.5.4

Features: 
- Nested Flexboxes
- Logo on the left
- Menu items right aligned
- Hamburger menu with svg strokes (for easy animation)
- Items Vertically centered
- Hover and toggle animations for Hamburger menu. This needs GSAP:

    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.1/TweenMax.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.1/plugins/ScrollToPlugin.min.js"></script>