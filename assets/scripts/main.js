/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages

        /*  Hamburger button animation
        *   Needs GSAP
        */

        $strokeColor = '#000';

        $hamburger = $('.hamburger');
        $line1 = $('.hamburger .line1');
        $line2 = $('.hamburger .line2');
        $line3 = $('.hamburger .line3');

        lines = [$line1, $line2, $line3];

        hamburgerHoverAnim = new TimelineMax({paused: true});
        hamburgerHoverAnim.staggerTo(lines, 0.25, {
          stroke: $strokeColor, scaleX: 1.1, repeat: 1, yoyo: true, ease: Power2.easeInOut, svgOrigin: "25 25"
        }, 0.125);

        $hamburger.mouseenter(function(){
          if ($hamburger.hasClass('js-open')){
            return;
          }
          hamburgerHoverAnim.play(0);
        });

        hamburgerAnim = new TimelineMax({ paused: true, reversed: true});

          hamburgerAnim
            .to($line2, 0.1, {scaleX: 0}, 0)
            .to($line1, 0.2, {transformOrigin: "50% 50%", y:8, ease: Power2.easeInOut}, "slide")
            .to($line3, 0.2, {transformOrigin: "50% 50%", y:-8, ease: Power2.easeInOut}, "slide")
            // .to($hamburger, 0.5, {rotation: 360, ease: Power4.easeInOut})
            .to($line1, 0.2, {rotation: 45,  ease: Power2.easeInOut}, "cross")
            .to($line3, 0.2, {rotation: -45, ease: Power2.easeInOut}, "cross");

        $hamburger.click(function(){
          hamburgerHoverAnim.pause(0);
          /*jshint ignore:start*/
          hamburgerAnim.reversed() ? hamburgerAnim.play() : hamburgerAnim.reverse();
          /*jshint ignore:end*/
          $hamburger.toggleClass('js-open');
          // $navbarCollapse.slideToggle(500, function () {
          //   $(this).toggleClass('show');
          // });
        });

      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
