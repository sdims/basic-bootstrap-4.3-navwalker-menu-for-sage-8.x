<?php use Roots\Sage; ?>

<header>
  <nav class="navbar navbar-expand-md">
    <div class="container navbar-container">
      <div class="navbar-brand-container">
        <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>">
          <div id="logo">

          </div>
        </a>
      </div>

      <div class="navbar-toggler-container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <svg class="hamburger" xlmns="https://www.w3.org/2000/svg" viewBox="0 0 50 50" width="50" height="50">
            <line class="line1" x1="12.5" y1="16" x2="37.5" y2="16" fill="none" stroke="12769e" stroke-miterlimit="10" stroke-width="3" />
            <line class="line2" x1="12.5" y1="24" x2="37.5" y2="24" fill="none" stroke="12769e" stroke-miterlimit="10" stroke-width="3" />
            <line class="line3" x1="12.5" y1="32" x2="37.5" y2="32" fill="none" stroke="12769e" stroke-miterlimit="10" stroke-width="3" />
          </svg>
        </button>
      </div>

      <div class="collapse navbar-collapse" id="navbarCollapse">
        <?php
        if (has_nav_menu('primary_navigation')) :
        wp_nav_menu([
          'theme_location' => 'primary_navigation',
          'menu_class' => 'navbar-nav',
          'walker' => new wp_bootstrap4_navwalker()
        ]);
        endif;
        ?>
        <!-- <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form> -->
      </div>
    <div>
  </nav>
  <div class="banner">

  </div>
</header>
